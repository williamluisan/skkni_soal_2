<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
        <i class="fa fa-arrows"></i>&nbsp;
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  <?php

    // flashdata error atau sukses
    if (! empty($this->session->flashdata('error'))) {
      echo $this->session->flashdata('error');
    }
  ?>
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Title</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-primary btn-md btn-sm" data-toggle="modal" data-target="#mdl-input"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah Peserta</button>
        </div>
      </div>
      <div class="box-body">
        <table class="table">
            <thead>
                <tr>
                    <th style="text-align: center;">No.</th>
                    <th style="text-align: center;">Tanggal Lahir</th>
                    <th style="text-align: center;">Jumlah Peserta</th>
                </tr>
            </thead>
            <tbody>
            <?php if (! empty($result)) :?>
                <?php foreach($result as $row) : ?>
                  <tr>
                      <td align="center"><?php echo $row['id'] ?></td>
                      <td align="center"><?php echo $row['tanggal_lahir'] ?></td>
                      <td align="center">
                          <label class="label label-lg label-primary" style="font-size: 12px;"><?php echo $row['jumlah'] ?></label>
                      </td>
                  </tr>
                <?php endforeach ?>
            <?php endif ?>
            </tbody>
        </table>
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modals -->
  <div class="modal fade modal-default" id="mdl-input">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="fa fa-edit"></i>&nbsp;&nbsp;Masukan Data Peserta</h4>
        </div>
        <?php echo form_open('Data/input');?>
        <div class="modal-body">
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="in-nama" name="in_nama" placeholder="Nama" required="required">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">NIK</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="in-nik" name="in_nik" placeholder="NIK" required="required">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">HP</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="in-hp" name="in_hp" placeholder="Nomor Handphone">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">E-Mail</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="in-mail" name="in_mail" placeholder="E-Mail">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Skema Sertifikasi</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="in-skema" name="in_skema" placeholder="Skema Sertifikasi" required="required">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Rekomendasi</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="in-rekomendasi" name="in_rekomendasi" placeholder="Rekomendasi">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Tanggal Terbit</label>
            <div class="col-sm-10">
              <input type="text" class="form-control datepicker" id="in-tgl-terbit" name="in_tgl_terbit" placeholder="Tanggal Terbit">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Tanggal Lahir</label>
            <div class="col-sm-10">
              <input type="text" class="form-control datepicker" id="in-tgl-lahir" name="in_tgl_lahir" placeholder="Tangggal Lahir" required="required">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Organisasi</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="in-organisasi" name="in_organisasi" placeholder="Organisasi">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
          <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;&nbsp;Simpan</button>
        </div>
        <?php echo form_close();?>
      </div>
    </div>
  </div>
<!-- // Modals -->
